#Requirement 0
    #Importing Random

from random import randint

#Requirement 1
    #Asking for name

print()
user_name = input("Hi! What is your name? " )

#Requirement 2
    #Guesses your birthday 5 times

for i in range(5):
    month = randint(1, 12)
    year = randint(1900, 2004)
    print()

    print(user_name + " were you born in " + str(month) + "/" + str(year) + "?")
    print()

    win_condition = input("Was my guess correct? Please answer with either Yes or No ")

    if win_condition == "Yes":
        print()
        print("I knew it!")
        break

    if win_condition == "No" and i < 4:
        print()
        print("Drat! Lemme try again!")

    if (win_condition != "Yes") and (win_condition != "No"):
        print()
        print("I did not understand that response, so I will guess again. Hope you are not using this loophole to cheat.")

print()
print("I have other things to do. Good bye.")
